#!/bin/bash

#small test
echo "1 1 2 8 15 7" | ./histogram

#large range test
echo "1 1 2 8 15 7 240 172 93 71" | ./histogram

#negative values test
echo "1 1 2 8 15 7 -3 50 31" | ./histogram

#bash test
echo {1..16} 150 | ./histogram