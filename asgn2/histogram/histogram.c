#include <stdio.h>
#include <stdlib.h>

// set bin const
#define BIN_LIM 16

void print_histogram(int bins[], int bin_size){
  for (int i = 0; i < BIN_LIM; i++){
    //sets bin size
    printf("[%2d: %2d] ", i * bin_size, (i + 1) * bin_size - 1);
    //lil point buddy
    for (int j = 0; j < bins[i]; j++){
      printf("*");
    }
    printf("\n");
  }
}

int main(){
  //init count
  int bins[BIN_LIM] = {0};
  int bin_size = 1;

  printf("16 bins of size 1 for range [0:16)\n");

  //help me
  int *numbers = (int *)malloc(sizeof(int) * 1000);
  int num_pointies = 0;

  int value;

  while (scanf("%d", &value) != EOF){
    if (value >= 0){

      while (value >= BIN_LIM * bin_size){
        //adapt range to new limit
        for (int i = 0; i < BIN_LIM / 2; i++){
          bins[i] += bins[i + BIN_LIM / 2];
        }
        //second half of bins reset
        for (int i = BIN_LIM / 2; i < BIN_LIM; i++){
          bins[i] = 0;
        }
        //
        bin_size *= 2;
        printf("16 bins of size %d for range [0:%d)\n", bin_size, BIN_LIM * bin_size);
      }
      if (num_pointies < 1000){
        numbers[num_pointies++] = value;
      }
    }
  }

//keep track of *s each bin
  for (int i = 0; i < num_pointies; i++){
    int bin_index = numbers[i] / bin_size;
    bins[bin_index]++;
  }

  print_histogram(bins, bin_size);
  free(numbers);

  return 0;
}
