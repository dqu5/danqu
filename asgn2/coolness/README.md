<h1>PART A - Coolness<h1>
Dan Qu 
dqu5@ucsc.edu

This project calculates "coolness" as a function of temperature and wind speed. 
The formula for this calculation is as follows:
35.74 + 0.6215*T* - 35.75*V*^0.16 + 0.4275*T* x *V*^0.16
Variable *T* being temperature and variable *V* being wind velocity
Accepted values for *T* are -99 to 50, and greater than 0.5 for *V*

The program can take up to two arguments, with the behavior as following:
1. For no arguments, will display *T* from -10 to 40 in steps of 10, *V* from 5 to 15 in steps of 5, and resulting coolness for each.
2. For one argument, always being *T*, will display *V* from 5 to 15 in steps of 5, and resulting coolness for each.
3. For two arguments, will display inputted *T*, *V*, and resulting coolness value.


*coolness.c* contains the source code for the program
*Makefile* contains dependencies and compiler
*testing.sh* runs a script to test three possible conditions for arguments, with edge case tests, with the resulting output in *testing.out*

For compiling, Makefile is available in commandline with command "make", or directly compile with gcc with commandline prompt "gcc coolness.c -o coolness.o"

To run, simply enter "./coolness" in commandline while within directory "coolness"
