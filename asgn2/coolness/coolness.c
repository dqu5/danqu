#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//expression func
double calculateCoolness(double temperature, double windSpeed){
  return 35.74 + 0.6215 * temperature - 35.75 * pow(windSpeed, 0.16) + 0.4275 * temperature * pow(windSpeed, 0.16);
}

int main(int argc, char *argv[]){

  //for two args
  if (argc == 3){
    double temperature = atof(argv[1]);
    double windSpeed = atof(argv[2]);

    //check args valid
    if (temperature >= -99.0 && temperature <= 50.0 && windSpeed >= 0.5){
      double coolness = calculateCoolness(temperature, windSpeed);

      //print columns
      printf("Temp\tWind\tCoolness\n");
      printf("%.1f\t%.1f\t%.1f\n", temperature, windSpeed, coolness);
      return 0;
    }
  }else if (argc == 2){ //for one arg (temp)
    double temperature = atof(argv[1]);

    //check args valid
    if (temperature >= -99.0 && temperature <= 50.0){
      printf("Temp\tWind\tCoolness\n");

      int lineCount = 0; //COUNTER HERE
      for (double windSpeed = 5.0; windSpeed <= 15.0; windSpeed += 5.0){
        double coolness = calculateCoolness(temperature, windSpeed);
        printf("%.1f\t%.1f\t%.1f\n", temperature, windSpeed, coolness);
        lineCount++;

        //every 3 for newline and notlast
        if (lineCount % 3 == 0 && windSpeed != 15.0){
          printf("\n");
        }
      }
      return 0;
    }
  }else if (argc == 1){ //no args condition
    printf("Temp\tWind\tCoolness\n");

    int lineCount = 0;
    for (double temperature = -10.0; temperature <= 40.0; temperature += 10.0){
      for (double windSpeed = 5.0; windSpeed <= 15.0; windSpeed += 5.0){
        double coolness = calculateCoolness(temperature, windSpeed);

        printf("%.1f\t%.1f\t%.1f\n", temperature, windSpeed, coolness);
        lineCount++;

        //see 39
        if (lineCount % 3 == 0 && temperature != 40.0){
          printf("\n");
        }
      }
    }
    return 0;
  }
  
  //invalid input check message
  if (atof(argv[1]) <= -99.0 || atof(argv[1]) >= 50.0 || atof(argv[2]) <= 0.5){
    fprintf(stderr, "Error: Coolness. Acceptable input values are -99<=T<=50 and 0.5<=V.\n");
  }else{ //invalid format check message
    fprintf(stderr, "Usage: %s [temperature] [wind speed]\n", argv[0]);
  }
  return 1;
}
