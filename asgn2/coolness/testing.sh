#!/bin/bash

#no arguments
echo "No arg test"
./coolness > noInputs.o

#one argument
echo "One arg test"
./coolness 32.0 > oneInput.o

#two argument
echo "Two (all) arg test"
./coolness 32.5 10.0 > twoInput.o

#out of range (high)
echo "Extreme high value test"
./coolness 100.0 0.6 > errorhigh.o

#out of range (low)
echo "Extreme low value test"
./coolness -100.0 -0.6 > errorlow.o

#arg format error
echo "Format error test"
./coolness 32.5 10.0 x > errorformat.o