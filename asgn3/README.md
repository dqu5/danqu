<h1>Assignment 3 - Password Checker<h1>

This project has me fixing the given password checking program. It lists a series of requirements that the program must meet, such as range, lower/upper case, and number requirements in the given password. For the program to be fixed, it must pass a series of tests given by test_password_checker.c, which ensures that the program correctly checks the given passwords for each of the previous requirements.

Makefile and test file are included.
The program can be compiled through "make" and ran by "./password_checker.c"
